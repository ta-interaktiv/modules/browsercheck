// @flow
/**
 * Provides various test cases to check for the browser capabilities.
 * @example
 * import {browserCheck} from '@ta-interaktiv/browsercheck'
 *
 * if (browserCheck.isIOS) {
 *   // do something iOS-specific here
 * }
 *
 * if (browserCheck.isAndroid) {
 *   // do something Android-specific here
 * }
 */
export class browserCheck {
  /**
   * Is the window width lower than 600px?
   * @returns {boolean}
   */
  static get isMobile (): boolean {
    return window.innerWidth < 600
  }

  /**
   * Is the window width between 600 and 900px?
   * @returns {boolean}
   */
  static get isTablet (): boolean {
    return window.innerWidth >= 600 && window.innerWidth < 900
  }

  /**
   * Is the window width larger than 900px?
   * @returns {boolean}
   */
  static get isDesktop (): boolean {
    return window.innerWidth >= 900
  }

  /**
   * Does the User Agent string contain «iPhone» or «iPod»?
   * @returns {boolean}
   */
  static get isIPhone (): boolean {
    return /i(Phone|Pod)/i.test(navigator.userAgent)
  }

  /**
   * Does the User Agent string contain «iPad»?
   * @returns {boolean}
   */
  static get isIPad (): boolean {
    return /iPad/i.test(navigator.userAgent)
  }

  /**
   * Does the User Agent string contain one of «iPhone», «iPad», or «iPod»?
   * @returns {boolean}
   */
  static get isIOS (): boolean {
    return browserCheck.isIPad || browserCheck.isIPhone
  }

  /**
   * Does the User Agent string contain «Android»?
   * @returns {boolean}
   */
  static get isAndroid (): boolean {
    return /Android/i.test(navigator.userAgent)
  }

  /**
   * Does the user agent string contain «app-ios-smartphone», as per iApp
   * user agent string specs.
   * @returns {boolean}
   */
  static get isIOSNativeApp (): boolean {
    return /app-ios-smartphone/i.test(navigator.userAgent)
  }

  /**
   * Does the user agent string contain «app-android-smartphone», as per
   * iApp user agent string specs.
   * @returns {boolean}
   */
  static get isAndroidNativeApp (): boolean {
    return /app-android-smartphone/i.test(navigator.userAgent)
  }

  /**
   *
   * @returns {boolean}
   */
  static get isNativeSmartphoneApp (): boolean {
    return browserCheck.isIOSNativeApp || browserCheck.isAndroidNativeApp
  }

  /**
   * Does the user agent string contain «app-ios-tablet»?
   * @returns {boolean}
   */
  static get isIOSNativeTabletApp (): boolean {
    return /app-ios-tablet/i.test(navigator.userAgent)
  }

  /**
   * Doest the user agent string contain «app-android-tablet»?
   * @returns {boolean}
   */
  static get isAndroidNativeTabletApp (): boolean {
    return /app-android-tablet/i.test(navigator.userAgent)
  }

  /**
   * Does the user agent suggest to be a native tablet app, either iOS or
   * Android?
   * @returns {boolean}
   */
  static get isNativeTabletApp (): boolean {
    return (
      browserCheck.isIOSNativeTabletApp || browserCheck.isAndroidNativeTabletApp
    )
  }

  /**
   * Is it one of the native apps, both smartphones and tablets.
   * @returns {boolean}
   */
  static get isNativeApp (): boolean {
    return browserCheck.isNativeSmartphoneApp || browserCheck.isNativeTabletApp
  }
}

export default browserCheck
